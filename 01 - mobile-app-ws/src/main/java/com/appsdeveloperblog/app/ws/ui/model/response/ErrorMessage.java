package com.appsdeveloperblog.app.ws.ui.model.response;

import java.time.LocalDateTime;

public class ErrorMessage {
	private LocalDateTime timeStamp;
	private String message;

	public ErrorMessage(LocalDateTime localDateTime, String message) {
		this.timeStamp = localDateTime;
		this.message = message;
	}

	public LocalDateTime getTimeStamp() {
		return timeStamp;
	}

	public void setTimeStamp(LocalDateTime timeStamp) {
		this.timeStamp = timeStamp;
	}

	public String getMessage() {
		return this.message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

}
