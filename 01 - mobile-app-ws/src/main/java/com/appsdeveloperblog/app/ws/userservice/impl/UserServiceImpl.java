package com.appsdeveloperblog.app.ws.userservice.impl;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.appsdeveloperblog.app.ws.shared.Utils;
import com.appsdeveloperblog.app.ws.ui.model.request.UserDetailsRequestModel;
import com.appsdeveloperblog.app.ws.ui.model.response.UserRest;
import com.appsdeveloperblog.app.ws.userservice.UserService;

@Service
public class UserServiceImpl implements UserService {

	Map<String, UserRest> users;

	Utils utils;

	public UserServiceImpl() {
	}

	@Autowired
	public UserServiceImpl(Utils utils) {
		this.utils = utils;
	}

	@Override
	public UserRest createUser(UserDetailsRequestModel userDetails) {

		UserRest rerturnValue = new UserRest();
		rerturnValue.setEmail(userDetails.getEmail());
		rerturnValue.setFirstName(userDetails.getFirstName());
		rerturnValue.setLastName(userDetails.getLastName());

		if (users == null)
			users = new HashMap<String, UserRest>();

		String userId = utils.generateUserId();

		rerturnValue.setUserId(userId);

		users.put(userId, rerturnValue);

		return rerturnValue;
	}

}
